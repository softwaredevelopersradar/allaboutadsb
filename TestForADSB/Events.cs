﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestForADSB
{
    public static class Events
    {
        public delegate void PlaneDataEventHandler(string ICAO, double Latitude, double Longitude, double Altitude);
        public static event PlaneDataEventHandler OnReceivePlaneData;

        public static void ReceivePlaneData(string ICAO, double Latitude, double Longitude, double Altitude)
        {
            if (OnReceivePlaneData != null)
            {
                OnReceivePlaneData (ICAO, Latitude, Longitude, Altitude);
            }
                
        }
    }
}
