﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public class CommDExtendedLengthMsg : ModeSReply
    {
        private sbyte[] message;
        private bool ack;
        private byte seqno;

        /** protected no-arg constructor e.g. for serialization with Kryo **/
        protected CommDExtendedLengthMsg() { }

        /**
         * @param raw_message raw comm-d extended len msg as hex string
         * @throws BadFormatException if message is not extended len msg or 
         * contains wrong values.
         */
        public CommDExtendedLengthMsg(String raw_message)
            : this(new CommDExtendedLengthMsg(raw_message))
        {
        }

        /**
         * @param reply Mode S reply which contains this extended len msg
         * @throws BadFormatException if message is not extended len msg or 
         * contains wrong values.
         */
        public CommDExtendedLengthMsg(ModeSReply reply) : base(reply)
        {
            setType(subtype.COMM_D_ELM);

            if (getDownlinkFormat() < 24)
            {
                //throw new BadFormatException("Message is not an extended length message!");
                Console.WriteLine("Message is not an extended length message!");
            }

            // extract Comm-D extended length message
            message = getPayload();
            ack = (getDownlinkFormat() & 0x2) != 0;
            seqno = (byte)((getDownlinkFormat() & 0x1) << 3 | getFirstField());
        }

        /**
         * @return the 10-byte Comm-D extended length message
         */
        public sbyte[] getMessage()
        {
            return message;
        }

        /**
         * @return true if this is a uplink ELM acknowledgement
         */
        public bool isAck()
        {
            return ack;
        }

        /**
         * @return the number of the message segment returned by {@link #getMessage()}
         */
        public byte getSequenceNumber()
        {
            return seqno;
        }

        public String toString()
        {
            return base.toString() + "\n" +
                    "Comm-D Extended Length Message:\n" +
                    "\tMessage is " + (isAck() ? "an" : "no") + " ACK\n" +
                    "\tSequence number: " + getSequenceNumber() + "\n" +
                    "\tComm-B Message:\t\t" + Tools.toHexString(getMessage());
        }
    }
}
