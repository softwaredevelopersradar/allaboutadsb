﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public static class Decoder
    {

        public static ModeSReply genericDecoder(String raw_message)
        {
            return genericDecoder(new ModeSReply(raw_message));
        }

        public static ModeSReply genericDecoder(String raw_message, bool noCRC)
        {
            return genericDecoder(new ModeSReply(raw_message, noCRC));
        }

        public static ModeSReply genericDecoder(ModeSReply modes)
        {
            switch (modes.getDownlinkFormat())
            {
                //TODO:case 0: return new ShortACAS(modes);
                //TODO:case 4: return new AltitudeReply(modes);
                //TODO:case 5: return new IdentifyReply(modes);
                //TODO:case 11: return new AllCallReply(modes);
                //TODO:case 16: return new LongACAS(modes);
                case 17:
                case 18:
                    ExtendedSquitter es1090 = new ExtendedSquitter(modes);

                    // what kind of extended squitter?
                    byte ftc = es1090.getFormatTypeCode();

                    if (ftc >= 1 && ftc <= 4) // identification message
                        //return new IdentificationMsg(es1090);

                    if (ftc >= 5 && ftc <= 8) // surface position message
                       // return new SurfacePositionMsg(es1090);

                    if ((ftc >= 9 && ftc <= 18) || (ftc >= 20 && ftc <= 22)) // airborne position message
                        return new AirbornePositionMsg(es1090);

                    if (ftc == 19)
                    { // possible velocity message, check subtype
                        int subtype = es1090.getMessage()[0] & 0x7;

                        if (subtype == 1 || subtype == 2) // velocity over ground
                            Console.WriteLine("VelocityOverGroundMsg not aviable at this moment, check if");
                        //return new VelocityOverGroundMsg(es1090);
                        else if (subtype == 3 || subtype == 4) // airspeed & heading
                            Console.WriteLine("AirspeedHeadingMsg not aviable at this moment, check if");
                        //return new AirspeedHeadingMsg(es1090);
                    }

                    if (ftc == 28)
                    { // aircraft status message, check subtype
                        int subtype = es1090.getMessage()[0] & 0x7;

                        if (subtype == 1) // emergency/priority status
                            //return new EmergencyOrPriorityStatusMsg(es1090);
                             Console.WriteLine("EmergencyOrPriorityStatusMsg not aviable at this moment, check if");
                        if (subtype == 2) // TCAS resolution advisory report
                                Console.WriteLine("TCASResolutionAdvisoryMsg not aviable at this moment, check if");
                        //return new TCASResolutionAdvisoryMsg(es1090);
                    }

                    if (ftc == 31)
                    { // operational status message
                        int subtype = es1090.getMessage()[0] & 0x7;

                        if (subtype == 0 || subtype == 1) // airborne or surface?
                            Console.WriteLine("OperationalStatusMsg not aviable at this moment, check if");
                            //return new OperationalStatusMsg(es1090);
                    }

                    return es1090; // unknown extended squitter
                //case 19: return new MilitaryExtendedSquitter(modes);
                //case 20: return new CommBAltitudeReply(modes);
                //case 21: return new CommBIdentifyReply(modes);
                default:
                    if (modes.getDownlinkFormat() >= 24)
                        return new CommDExtendedLengthMsg(modes);
                    else return modes; // unknown mode s reply
            }
        }
    }
}
