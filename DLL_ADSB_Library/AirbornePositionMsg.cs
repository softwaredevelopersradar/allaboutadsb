﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public class AirbornePositionMsg : ExtendedSquitter
    {
        private bool horizontal_position_available;
        private bool altitude_available;
        private byte surveillance_status;
        private bool nic_suppl_b;
        private short altitude_encoded;
        private bool time_flag;
        private bool cpr_format;
        private int cpr_encoded_lat;
        private int cpr_encoded_lon;
        private bool nic_suppl_a;

        /** protected no-arg constructor e.g. for serialization with Kryo **/
        protected AirbornePositionMsg() { }

        /**
         * @param raw_message raw ADS-B airborne position message as hex string
         * @throws BadFormatException if message has wrong format
         */
        public AirbornePositionMsg(String raw_message)
            : this(new ExtendedSquitter(raw_message))
        {
        }

        /**
         * @param squitter extended squitter containing the airborne position msg
         * @throws BadFormatException if message has wrong format
         */
        public AirbornePositionMsg(ExtendedSquitter squitter) : base(squitter)
        {
            setType(subtype.ADSB_AIRBORN_POSITION);

            if (!(getFormatTypeCode() == 0 ||
                    (getFormatTypeCode() >= 9 && getFormatTypeCode() <= 18) ||
                    (getFormatTypeCode() >= 20 && getFormatTypeCode() <= 22)))
                //throw new BadFormatException("This is not a position message! Wrong format type code ("+getFormatTypeCode()+").");
                Console.WriteLine("This is not a position message! Wrong format type code (" + getFormatTypeCode() + ").");

            sbyte[] msg = getMessage();

            horizontal_position_available = getFormatTypeCode() != 0;

            surveillance_status = (byte)((msg[0] >> 1) & 0x3);
            nic_suppl_b = (msg[0] & 0x1) == 1;

            altitude_encoded = (short)(((msg[1] << 4) | ((msg[2] >> 4) & 0xF)) & 0xFFF);
            altitude_available = altitude_encoded != 0;

            time_flag = ((msg[2] >> 3) & 0x1) == 1;
            cpr_format = ((msg[2] >> 2) & 0x1) == 1;
            cpr_encoded_lat = (((msg[2] & 0x3) << 15) | ((msg[3] & 0xFF) << 7) | ((msg[4] >> 1) & 0x7F)) & 0x1FFFF;
            cpr_encoded_lon = (((msg[4] & 0x1) << 16) | ((msg[5] & 0xFF) << 8) | (msg[6] & 0xFF)) & 0x1FFFF;
        }

        /**
         * @return NIC supplement that was set before
         */
        public bool getNICSupplementA()
        {
            return nic_suppl_a;
        }


        /**
         * @param nic_suppl Navigation Integrity Category (NIC) supplement from operational status message.
         *        Otherwise worst case is assumed for containment radius limit and NIC.
         */
        public void setNICSupplementA(bool nic_suppl)
        {
            this.nic_suppl_a = nic_suppl;
        }

        /**
         * @return horizontal containment radius limit in meters. A return value of -1 means "unkown".
         *         Set NIC supplement A from Operational Status Message for better precision.
         *         Otherwise, we'll be pessimistic.
         *         Note: For ADS-B versions &lt; 2, this is inaccurate for NIC class 6, since there was
         *         no NIC supplement B in earlier versions.
         */
        public double getHorizontalContainmentRadiusLimit()
        {
            switch (getFormatTypeCode())
            {
                case 0: case 18: case 22: return -1;
                case 9: case 20: return 7.5;
                case 10: case 21: return 25;
                case 11:
                    return nic_suppl_b ? 75 : 185.2;
                case 12: return 370.4;
                case 13:
                    if (!nic_suppl_b) return 926;
                    else return nic_suppl_a ? 1111.2 : 555.6;
                case 14: return 1852;
                case 15: return 3704;
                case 16:
                    return nic_suppl_b ? 7408 : 14816;
                case 17: return 37040;
                default: return -1;
            }
        }

        /**
         * @return Navigation integrity category. A NIC of 0 means "unkown".
         */
        public byte getNavigationIntegrityCategory()
        {
            switch (getFormatTypeCode())
            {
                case 0: case 18: case 22: return 0;
                case 9: case 20: return 11;
                case 10: case 21: return 10;
                case 11:
                    return (byte)(nic_suppl_b ? 9 : 8);
                case 12: return 7;
                case 13: return 6;
                case 14: return 5;
                case 15: return 4;
                case 16:
                    return (byte)(nic_suppl_b ? 3 : 2);
                case 17: return 1;
                default: return 0;
            }
        }

        /**
         * @return whether horizontal position information is available
         */
        public bool hasPosition()
        {
            return horizontal_position_available;
        }

        /**
         * @return whether altitude information is available
         */
        public bool hasAltitude()
        {
            return altitude_available;
        }

        /**
         * @see #getSurveillanceStatusDescription()
         * @return the surveillance status
         */
        public byte getSurveillanceStatus()
        {
            return surveillance_status;
        }

        /**
         * This is a function of the surveillance status field in the position
         * message.
         * 
         * @return surveillance status description as defines in DO-260B
         */
        public String getSurveillanceStatusDescription()
        {
            String[] desc = {
                "No condition information",
                "Permanent alert (emergency condition)",
                "Temporary alert (change in Mode A identity code oter than emergency condition)",
                "SPI condition"
        };

            return desc[surveillance_status];
        }

        /**
         * @return for ADS-B version 0 and 1 messages true, iff transmitting system uses only one antenna.
         *         For ADS-B version 2, this flag represents the NIC supplement B!
         */
        public bool getNICSupplementB()
        {
            return nic_suppl_b;
        }

        /**
         * @return flag which will indicate whether or not the Time of Applicability of the message
         *         is synchronized with UTC time. False will denote that the time is not synchronized
         *         to UTC. True will denote that Time of Applicability is synchronized to UTC time.
         */
        public bool getTimeFlag()
        {
            return time_flag;
        }

        /**
         * @return the CPR encoded binary latitude
         */
        public int getCPREncodedLatitude()
        {
            return cpr_encoded_lat;
        }

        /**
         * @return the CPR encoded binary longitude
         */
        public int getCPREncodedLongitude()
        {
            return cpr_encoded_lon;
        }

        /**
         * @return whether message is odd format. Returns false if message is even format. This is
         *         needed for position decoding as the CPR algorithm uses both formats.
         */
        public bool isOddFormat()
        {
            return cpr_format;
        }

        /**
         * @return true, if barometric altitude. False if GNSS is used to determine altitude
         */
        public bool isBarometricAltitude()
        {
            return this.getFormatTypeCode() < 20;
        }

        /**
         * @param Rlat Even or odd Rlat value (CPR internal)
         * @return the number of even longitude zones at a latitude
         */
        private double NL(double Rlat)
        {
            if (Rlat == 0) return 59;
            else if (Math.Abs(Rlat) == 87) return 2;
            else if (Math.Abs(Rlat) > 87) return 1;

            double tmp = 1 - (1 - Math.Cos(Math.PI / (2.0 * 15.0))) / Math.Pow(Math.Cos(Math.PI / 180.0 * Math.Abs(Rlat)), 2);
            return Math.Floor(2 * Math.PI / Math.Acos(tmp));
        }

        /**
         * Modulo operator in java has stupid behavior
         */
        private static double mod(double a, double b)
        {
            return ((a % b) + b) % b;
        }

        /**
         * This method can only be used if another position report with a different format (even/odd) is available
         * and set with msg.setOtherFormatMsg(other).
         * @param other airborne position message of the other format (even/odd). Note that the time between
         *        both messages should be not longer than 10 seconds! 
         * @return globally unambiguously decoded position. The positional
         *         accuracy maintained by the Airborne CPR encoding will be approximately 5.1 meters.
         *         A message of the other format is needed for global decoding.
         * @throws MissingInformationException if no position information is available in one of the messages
         * @throws IllegalArgumentException if input message was emitted from a different transmitter
         * @throws PositionStraddleError if position messages straddle latitude transition
         * @throws BadFormatException other has the same format (even/odd)
         */
        public Position getGlobalPosition(AirbornePositionMsg other)
        {
            if (!Tools.areEqual(other.getIcao24(), getIcao24()))
                //throw new IllegalArgumentException(
                //        String.format("Transmitter of other message (%s) not equal to this (%s).",
                //        tools.toHexString(other.getIcao24()), tools.toHexString(this.getIcao24())));
                Console.WriteLine(String.Format("Transmitter of other message (%s) not equal to this (%s).",
                        Tools.toHexString(other.getIcao24()), Tools.toHexString(this.getIcao24())));

            if (other.isOddFormat() == this.isOddFormat())
                //throw new BadFormatException("Expected " + (isOddFormat() ? "even" : "odd") + " message format.", other.toString());
                Console.WriteLine("Expected " + (isOddFormat() ? "even" : "odd") + " message format.", other.toString());

            if (!horizontal_position_available)
                // throw new MissingInformationException("No position information available!");
                Console.WriteLine("No position information available!");
            if (!other.hasPosition())
                //throw new MissingInformationException("Other message has no position information.");
                Console.WriteLine("Other message has no position information.");

            AirbornePositionMsg even = isOddFormat() ? other : this;
            AirbornePositionMsg odd = isOddFormat() ? this : other;

            // Helper for latitude (Number of zones NZ is set to 15)
            double Dlat0 = 360.0 / 60.0;
            double Dlat1 = 360.0 / 59.0;

            // latitude index
            double j = Math.Floor((59.0 * even.getCPREncodedLatitude() - 60.0 * odd.getCPREncodedLatitude()) / ((double)(1 << 17)) + 0.5);

            // global latitudes
            double Rlat0 = Dlat0 * (mod(j, 60) + even.getCPREncodedLatitude() / ((double)(1 << 17)));
            double Rlat1 = Dlat1 * (mod(j, 59) + odd.getCPREncodedLatitude() / ((double)(1 << 17)));

            // Southern hemisphere?
            if (Rlat0 >= 270 && Rlat0 <= 360) Rlat0 -= 360;
            if (Rlat1 >= 270 && Rlat1 <= 360) Rlat1 -= 360;

            // Northern hemisphere?
            if (Rlat0 <= -270 && Rlat0 >= -360) Rlat0 += 360;
            if (Rlat1 <= -270 && Rlat1 >= -360) Rlat1 += 360;

            // ensure that the number of even longitude zones are equal
            if (NL(Rlat0) != NL(Rlat1))
                //throw new org.opensky.libadsb.exceptions.PositionStraddleError(
                //    "The two given position straddle a transition latitude " +
                //    "and cannot be decoded. Wait for positions where they are equal.");
                Console.WriteLine("The two given position straddle a transition latitude " +
                "and cannot be decoded. Wait for positions where they are equal.");

            // Helper for longitude
            double Dlon0 = 360.0 / Math.Max(1.0, NL(Rlat0));
            double Dlon1 = 360.0 / Math.Max(1.0, NL(Rlat1) - 1);

            // longitude index
            double NL_helper = NL(isOddFormat() ? Rlat1 : Rlat0); // assuming that this is the newer message
            double m = Math.Floor((even.getCPREncodedLongitude() * (NL_helper - 1) - odd.getCPREncodedLongitude() * NL_helper) / ((double)(1 << 17)) + 0.5);

            // global longitude
            double Rlon0 = Dlon0 * (mod(m, Math.Max(1.0, NL(Rlat0))) + even.getCPREncodedLongitude() / ((double)(1 << 17)));
            double Rlon1 = Dlon1 * (mod(m, Math.Max(1.0, NL(Rlat1) - 1)) + odd.getCPREncodedLongitude() / ((double)(1 << 17)));

            // correct longitude
            if (Rlon0 < -180 && Rlon0 > -360) Rlon0 += 360;
            if (Rlon1 < -180 && Rlon1 > -360) Rlon1 += 360;
            if (Rlon0 > 180 && Rlon0 < 360) Rlon0 -= 360;
            if (Rlon1 > 180 && Rlon1 < 360) Rlon1 -= 360;

            return new Position(isOddFormat() ? Rlon1 : Rlon0,
                                isOddFormat() ? Rlat1 : Rlat0,
                                this.hasAltitude() ? this.getAltitude() : 0);
        }

        /**
         * This method uses a locally unambiguous decoding for airborne position messages. It
         * uses a reference position known to be within 180NM (= 333.36km) of the true target
         * airborne position. the reference point may be a previously tracked position that has
         * been confirmed by global decoding (see getGlobalPosition()).
         * @param ref reference position
         * @return decoded position. The positional
         *         accuracy maintained by the Airborne CPR encoding will be approximately 5.1 meters.
         * @throws MissingInformationException if no position information is available
         */
        public Position getLocalPosition(Position ref1)
        {
            if (!horizontal_position_available)
                //throw new MissingInformationException("No position information available!");
                Console.WriteLine("No position information available!");

            // latitude zone size
            double Dlat = isOddFormat() ? 360.0 / 59.0 : 360.0 / 60.0;

            // latitude zone index
            double j = Math.Floor(ref1.getLatitude() / Dlat) +
                    Math.Floor(0.5 + (mod(ref1.getLatitude(), Dlat)) / Dlat - getCPREncodedLatitude() / ((double)(1 << 17)));

            // decoded position latitude
            double Rlat = Dlat * (j + getCPREncodedLatitude() / ((double)(1 << 17)));

            // longitude zone size
            double Dlon = 360.0 / Math.Max(1.0, NL(Rlat) - (isOddFormat() ? 1.0 : 0.0));

            // longitude zone coordinate
            double m =
                    Math.Floor(ref1.getLongitude() / Dlon) +
                    Math.Floor(0.5 + (mod(ref1.getLongitude(), Dlon)) / Dlon - (double)getCPREncodedLongitude() / ((double)(1 << 17)));

            // and finally the longitude
            double Rlon = Dlon * (m + getCPREncodedLongitude() / ((double)(1 << 17)));

            return new Position(Rlon, Rlat, this.hasAltitude() ? this.getAltitude() : 0);
        }

        /**
         * This method converts a gray code encoded int to a standard decimal int
         * @param gray gray code encoded int of length bitlength
         *        bitlength bitlength of gray code
         * @return radix 2 encoded integer
         */
        private static int grayToBin(int gray, int bitlength)
        {
            int result = 0;
            for (int i = bitlength - 1; i >= 0; --i)
                result = result | ((((0x1 << (i + 1)) & result) >> 1) ^ ((1 << i) & gray));
            return result;
        }

        /**
         * @return the decoded altitude in meters
         * @throws MissingInformationException if no position available
         */
        public double getAltitude()
        {
            if (!altitude_available)
                //throw new MissingInformationException("No altitude information available!");
                Console.WriteLine("No altitude information available!");

            bool Qbit = (altitude_encoded & 0x10) != 0;
            int N;
            if (Qbit)
            { // altitude reported in 25ft increments
                N = (altitude_encoded & 0xF) | ((altitude_encoded & 0xFE0) >> 1);
                return (25 * N - 1000) * 0.3048;
            }
            else
            { // altitude is above 50175ft, so we use 100ft increments

                // it's decoded using the Gillham code
                int C1 = (0x800 & altitude_encoded) >> 11;
                int A1 = (0x400 & altitude_encoded) >> 10;
                int C2 = (0x200 & altitude_encoded) >> 9;
                int A2 = (0x100 & altitude_encoded) >> 8;
                int C4 = (0x080 & altitude_encoded) >> 7;
                int A4 = (0x040 & altitude_encoded) >> 6;
                int B1 = (0x020 & altitude_encoded) >> 5;
                int B2 = (0x008 & altitude_encoded) >> 3;
                int D2 = (0x004 & altitude_encoded) >> 2;
                int B4 = (0x002 & altitude_encoded) >> 1;
                int D4 = (0x001 & altitude_encoded);

                // this is standard gray code
                int N500 = grayToBin(D2 << 7 | D4 << 6 | A1 << 5 | A2 << 4 | A4 << 3 | B1 << 2 | B2 << 1 | B4, 8);

                // 100-ft steps must be converted
                int N100 = grayToBin(C1 << 2 | C2 << 1 | C4, 3) - 1;
                if (N100 == 6) N100 = 4;
                if (N500 % 2 != 0) N100 = 4 - N100; // invert it

                return (-1200 + N500 * 500 + N100 * 100) * 0.3048;
            }
        }

        //public String toString()
        //{
        //    try
        //    {
        //        return super.toString() + "\n" +
        //                "Position:\n" +
        //                "\tFormat:\t\t" + (isOddFormat() ? "odd" : "even") +
        //                "\n\tHas position:\t" + (hasPosition() ? "yes" : "no") +
        //                "\n\tAltitude:\t" + (hasAltitude() ? getAltitude() : "unkown");
        //    }
        //    catch (MissingInformationException e)
        //    {
        //        return "Position: Missing information!";
        //    }
        //}
    }
}
