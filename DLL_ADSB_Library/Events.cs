﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public static class Events
    {
        public delegate void PlaneDataEventHandler(string iCAO, double latitude, double longitude, double altitude);
        public static event PlaneDataEventHandler OnReceivePlaneData;

        public delegate void PlaneShortDataEventHandler(string iCAO, double altitude);
        public static event PlaneShortDataEventHandler OnReceiveShortPlaneData;

        public static void ReceivePlaneData(string iCAO, double latitude, double longitude, double altitude)
        {
            if (OnReceivePlaneData != null)
            {
                OnReceivePlaneData (iCAO, latitude, longitude, altitude);
            }
                
        }

        public static void ReceiveShortPlaneData(string iCAO, double altitude)
        {
            if (OnReceiveShortPlaneData != null)
            {
                OnReceiveShortPlaneData(iCAO,  altitude);
            }

        }
    }
}
