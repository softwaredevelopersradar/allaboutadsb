﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public class ExtendedSquitter : ModeSReply
    {
        private byte capabilities;
        private sbyte[] message;
        private byte format_type_code;


        /** protected no-arg constructor e.g. for serialization with Kryo **/
        protected ExtendedSquitter() { }

        /**
         * @param raw_message raw extended squitter as hex string
         * @throws BadFormatException if message is not extended squitter or 
         * contains wrong values.
         */
        public ExtendedSquitter(String raw_message)
            : this(new ModeSReply(raw_message))
        {

        }

        /**
         * @param reply Mode S reply containing this extended squitter
         * @throws BadFormatException if message is not extended squitter or 
         * contains wrong values.
         */
        public ExtendedSquitter(ModeSReply reply) : base(reply)
        {
            //base(reply);
            setType(subtype.EXTENDED_SQUITTER);

            if (getDownlinkFormat() != 17 && getDownlinkFormat() != 18)
            {
                //TODO: throw new BadFormatException("Message is not an extended squitter!");
                Console.WriteLine("Message is not an extended squitter!");
            }

            sbyte[] payload = getPayload();
            capabilities = getFirstField();

            // extract ADS-B message
            message = new sbyte[7];
            for (int i = 0; i < 7; i++)
                message[i] = payload[i + 3];

            format_type_code = (byte)((message[0] >> 3) & 0x1F);
        }

        /**
         * Copy constructor for subclasses
         * 
         * @param squitter instance of ExtendedSquitter to copy from
         */
        public ExtendedSquitter(ExtendedSquitter squitter) : base(squitter)
        {
            //super(squitter);

            capabilities = squitter.getCapabilities();
            message = squitter.getMessage();
            format_type_code = squitter.getFormatTypeCode();
        }

        /**
         * @return The emitter's capabilities (see ICAO Annex 10 V4; 3.1.2.5.2.2.1)
         */
        public byte getCapabilities()
        {
            return capabilities;
        }

        /**
         * @return The message's format type code (see ICAO Annex 10 V4)
         */
        public byte getFormatTypeCode()
        {
            return format_type_code;
        }

        /**
         * @return The message as 7-byte array
         */
        public sbyte[] getMessage()
        {
            return message;
        }

        public String toString()
        {
            return base.toString() + "\n" +
                    "Extended Squitter:\n" +
                    "\tFormat type code:\t" + getFormatTypeCode() + "\n" +
                    "\tCapabilities:\t\t" + getCapabilities() + "\n" +
                    "\tMessage field:\t\t" + Tools.toHexString(getMessage());
        }




    }
}
