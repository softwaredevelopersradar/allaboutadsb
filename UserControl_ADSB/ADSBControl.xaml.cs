﻿using DLL_ADSB_Library;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ADSBControl
{

    /// <summary>
    /// Interaction logic for UserControlADSB.xaml
    /// </summary>
    public partial class ADSBControl : UserControl
    {
        //private ADSB_Decoder ADSBDecoder;
        private ApplicationViewModel curVM;

        public ADSBControl()
        {

            InitializeComponent();
            this.SizeChanged += OnWindowSizeChanged;
            curVM = new ApplicationViewModel();
            DataContext = curVM;

        
            // testName.ContentTemplateSelector = new BitmapImage(new Uri(@"D:\Print.png"));

            //testName.Source = new BitmapImage( new Uri("pack://application:,,,/WClassLibrary1;component/CountryFlags/France.png"));


            //ADSBDecoder = new ADSB_Decoder();
            //Task.Run(() => ADSBDecoder.ConnectToADSB());
        }


        protected void OnWindowSizeChanged(object sender, SizeChangedEventArgs e)
        {
            double newwindowheight = e.NewSize.Height;
            //double prevwindowheight = e.previoussize.height;
            curVM.DrawEmptyRows(newwindowheight);
            Task.Delay(100);
        }

        //public  void ADSBDecoder_Disconnect()
        //{
        //    ADSBDecoder.Disconnect();
        //}

        public void AddPlaneToTable(string curICAO, double curLatitude, double curLongitude, double curAltitude)
        {
            curVM.AddPlaneToTable(curICAO, curLatitude, curLongitude, curAltitude);
        }

        public void AddRange(List<TempADSB> planeList)
        {
            curVM.AddRange(planeList);
        }

        public void DeleteR(string curIcao)
        {
            curVM.DeleteRecord(curIcao);
        }
    }
}
