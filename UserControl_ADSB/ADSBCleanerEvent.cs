﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADSBControl
{
    public static class ADSBCleanerEvent
    {
        public static event EventHandler OnCleanADSBTable;

        public static void CleanADSBTable(object sender, EventArgs param)
        {
            Clean(sender, param);
        }

        private static void Clean(object sender, EventArgs param)
        {
            if (OnCleanADSBTable != null)
            {
                OnCleanADSBTable(sender, param);
            }
        }

    }
}
