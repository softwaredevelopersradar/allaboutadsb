﻿
using ADSBControl;
using System;
using System.ComponentModel;
using System.Windows;
using TempADSBLibrary;

namespace DarkOrangeADS_b
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            ////Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindow_Closing);
            //var ADSBDecoder = new ADSB_Decoder();
            //Task.Run(() => ADSBDecoder.ConnectToADSB());
            //Events.OnReceivePlaneData += AddPlaneToTable;
            //Events.OnReceiveShortPlaneData += AddPlaneToTable2;

            DataDecoder.ConnectADSB("192.168.0.11", 30005);
            TempEvents.OnReceivePlaneData += AddPlaneToTable;
            TempEvents.OnReceiveShortPlaneData += AddPlaneToTable2;


        }

        public void AddPlaneToTable2(string curICAO, double curAltitude)
        {
            ADSBControl.AddPlaneToTable(curICAO,-1,-1, curAltitude);
        }

        private void AddPlaneToTable(string iCAO, double latitude, double longitude, double altitude)
        {
            ADSBControl.AddPlaneToTable( iCAO,  latitude,  longitude,  altitude);
            curICAO = iCAO;
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            //UserControlADSB.ADSBDecoder_Disconnect();
        }

        string curICAO;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ADSBControl.DeleteR(curICAO);
        }
    }
}
