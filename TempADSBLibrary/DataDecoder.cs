﻿using DeviceLib;
using DeviceLib.DataSource;
using DeviceLib.HeaderSplitting;
using DeviceLib.Listenning;
using DeviceLib.MessageSplitting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace TempADSBLibrary
{
    public static class DataDecoder
    {

        public delegate void IndReadADSBEventHandler();
        public static event IndReadADSBEventHandler IndReadADSB;

        static Struct_ADSB_Receiver struct_ADSB_Receiver;
        static List<TDataADSBReceiver> listADSB;
        public static List<Struct_ADSB_Receiver> listStructADSB;

        public static DeviceHolder<string> deviceHolder;
        public static PassiveListener<string> allMessageListener;
        public static TcpDataSource data_source;

        private static bool fDF = false;
        public static int created;



        public static void ConnectADSB(string Ip, int port)
        {

            struct_ADSB_Receiver = new Struct_ADSB_Receiver();
            listADSB = new List<TDataADSBReceiver>();
            listStructADSB = new List<Struct_ADSB_Receiver>();
            //create device holder
            IEnumerable<byte> bMas = new List<byte>() { 0x3b, 0x0a, 0x0d };
            deviceHolder = new DeviceHolder<string>(new EndLineMessageSplitter(bMas), new EmptyGPSHeaderSplitter());

            deviceHolder.onConnectionStatusChanged += new EventHandler<bool>(deviceHolder_onConnectionStatusChanged);

            if (!deviceHolder.IsConnected)
            {
                //connect listener
                allMessageListener = new PassiveListener<string> { ID = string.Empty };
                allMessageListener.onDataArrived += new EventHandler<IEnumerable<byte>>(allMessageListener_onDataArrived);
                allMessageListener.Start();
                deviceHolder.Subscribe(allMessageListener);


                //data_source = new TcpDataSource(new IPEndPoint(IPAddress.Parse("192.168.0.11"), 30005)) { ReadTimeout = 500 };
                data_source = new TcpDataSource(new IPEndPoint(IPAddress.Parse(Ip), port)) { ReadTimeout = 500 };

                try
                {
                    deviceHolder.Open(data_source);
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message, SMessageError.mesErr, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                created = DecoderDLL.CreateDecoder(1800, 0); // функция создает класс

            }
        }

        private static void deviceHolder_onConnectionStatusChanged(object sender, bool e)
        {
            //Console.WriteLine("ADSB_Receiver connected: {0}", e);
            TempEvents.IsConnected(e);

        }

        private static void allMessageListener_onDataArrived(object sender, IEnumerable<byte> bMas)
        {
            StringBuilder sbDecoder = new StringBuilder();
            string sMasHEX = "";
            string s5Bit = "";
            sbDecoder.Append("1,");

            byte[] mess = bMas.ToArray();

            if (mess.Length < 18)
                return;

            sMasHEX = Encoding.ASCII.GetString(mess);
            sMasHEX = new Regex(@"[^0-9ABCDEF]").Replace(sMasHEX, ""); // удалить все символы кроме цифр и букв

            string sDF = sMasHEX.Substring(0, 2);

            int num = Int32.Parse(sDF, System.Globalization.NumberStyles.HexNumber);

            BitArray ba = new BitArray(new byte[] { Convert.ToByte(num) });
            s5Bit = Convert.ToByte(ba[7]).ToString() + Convert.ToByte(ba[6]).ToString() + Convert.ToByte(ba[5]).ToString() + Convert.ToByte(ba[4]).ToString() + Convert.ToByte(ba[3]).ToString();

            if ((sMasHEX.Length == 28) || (sMasHEX.Length == 14))
            {


                switch (s5Bit)
                {
                    //case "00000": // DF0
                    //case "00100": // DF4
                    //case "00101": // DF5
                    //case "01011": // DF11
                    //case "10000": // DF16
                    case "10001": // DF17
                    case "10010": // DF18
                                  //case "10011": // DF19
                                  //case "10100": // DF20
                                  //case "10101": // DF21
                                  //case "00011": // DF24
                        fDF = true;
                        break;
                }

                if (fDF)
                {
                    sbDecoder.Append(sMasHEX + ",");
                    //my own test
                    Console.WriteLine(sMasHEX);
                    Console.WriteLine("");
                    //

                    //my own test
                    //string ss = "8d4ca513587153a8184a2fb6adeb";
                    //sbDecoder.Append(sMasHEX + ",");
                    //

                    string dt = DateTime.Now.ToString(@"yyyy/MM/dd HH:mm:ss.fff");

                    sbDecoder.Append(dt + ",,,,\r\n");

                    try
                    {
                        StringBuilder sb = new StringBuilder(100000);
                        if (created > 0)
                        {
                            if (IndReadADSB != null)
                                IndReadADSB();
                            //IndicateReadDataADSB();

                            DecoderDLL.Decode(sbDecoder.ToString(), sb);



                            string[] strDecode = sb.ToString().Split(',');
                            //strDecode[1] = "8d4b19f39911088090641010b9b0";

                            struct_ADSB_Receiver.sICAO = strDecode[1].Substring(2, 6); // ICAO
                            struct_ADSB_Receiver.sDatetime = strDecode[2];  // Date, time
                            struct_ADSB_Receiver.sLatitude = strDecode[7]; //.ToString(nfi);  // Latitude, deg
                            struct_ADSB_Receiver.sLongitude = strDecode[8]; //.ToString(nfi); // Longitude, deg
                            struct_ADSB_Receiver.sAltitude = strDecode[9]; //.ToString(nfi);  // Altitude, m



                            //if (!struct_ADSB_Receiver.sLatitude.Equals("") && !struct_ADSB_Receiver.sLongitude.Equals(""))
                            //    if (double.Parse(struct_ADSB_Receiver.sLatitude, CultureInfo.InvariantCulture) > 48 &&
                            //            double.Parse(struct_ADSB_Receiver.sLatitude, CultureInfo.InvariantCulture) < 57)

                            //        if (double.Parse(struct_ADSB_Receiver.sLongitude, CultureInfo.InvariantCulture) > 18 &&
                            //            double.Parse(struct_ADSB_Receiver.sLongitude, CultureInfo.InvariantCulture) < 35)

                            if (!struct_ADSB_Receiver.sLatitude.Equals("") && !struct_ADSB_Receiver.sLongitude.Equals(""))
                                TempEvents.ReceivePlaneData(struct_ADSB_Receiver.sICAO,
                                double.Parse(struct_ADSB_Receiver.sLatitude, CultureInfo.InvariantCulture),
                                double.Parse(struct_ADSB_Receiver.sLongitude, CultureInfo.InvariantCulture),
                                double.Parse(struct_ADSB_Receiver.sAltitude, CultureInfo.InvariantCulture));

                            if (!struct_ADSB_Receiver.sAltitude.Equals(""))
                                TempEvents.ReceiveShortPlaneData(struct_ADSB_Receiver.sICAO,
                                double.Parse(struct_ADSB_Receiver.sAltitude, CultureInfo.InvariantCulture));

                        }
                        fDF = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public static void CloseConnection()
        {
            allMessageListener.onDataArrived -= new EventHandler<IEnumerable<byte>>(allMessageListener_onDataArrived);

            Thread.Sleep(300);
            deviceHolder.Close();
        }

    }
}
